// To compile & link: gcc -o ascii_sum ascii_sum.c ascii_sum.s

#include <stdio.h>

/**
* returns the sum of ascii values of str characters counting from a to b
* the function assumes that a <= b
* Only characters matching the charset are counted.
* * for charset == 0 every character is valid
* * for charset == 1 mask is [0-9]
* * for charset == 2 mask is [A-Z]
* * for charset == 3 mask is [a-z]
* * for charset == 4 mask is [A-Za-z]
* The number of chars counted is returned via count
**/
long long int sum_ascii(char* str, int a, int b, int charset, int* count);

int main()
{
	char* str = "abcABC1234567890";
	int a = 0;
	int b = 6;
	int charset = 0;
	int count;
	long long int sum = sum_ascii(str, a, b, charset, &count);
	printf("Ascii sum: %lld. Chars counted: %d\n", sum, count);
	return 0;
}