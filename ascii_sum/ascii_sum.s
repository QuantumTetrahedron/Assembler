	.text
	.type sum_ascii, @function
	.global sum_ascii

# rdi - str
# rsi - a
# rdx - b
# rcx - charset
# r8 - &count

sum_ascii:
	# b - a + 1 = ile liter brać pod uwagę
	sub %rsi, %rdx
	inc %rdx

	# str + a = adres pierwszej sprawdzanej litery
	add %rdi, %rsi
	
	# wyzerowanie rejestrów
	xor %rax, %rax	#wartość zwracana, przechowuje również czytane znaki
	xor %r10, %r10  #rejestr przechowujący sumę podczas obliczania. Nie jest to %rax bo LODSB wczytuje znak do %al
	xor %r11, %r11	#licznik znaków wliczonych w sumę

loop_start:
	lodsb
	
	#sprawdzanie końca łańcucha znaków
	cmpb $0, %al
	je end_loop
		
	#sprawdzenie charsetu
	cmp $0, %rcx
	je ok
	cmp $1, %rcx
	je numbers
	cmp $2, %rcx
	je big
	cmp $3, %rcx
	je small
	cmp $4, %rcx
	je letters
	
	#sprawdzenie czy znak powinien być włączony w sumę
numbers:
	cmpb $'0', %al
	jl skip
	cmpb $'9', %al
	jg skip
	jmp ok
big:
	cmpb $'A', %al
	jl skip
	cmpb $'Z', %al
	jg skip
	jmp ok
small:
	cmpb $'a', %al
	jl skip
	cmpb $'z', %al
	jg skip
	jmp ok
letters:
	cmpb $'A', %al
	jl skip
	cmpb $'z', %al
	jg skip
	cmpb $'Z', %al
	jle ok
	cmpb $'a', %al
	jge ok
	jmp skip

ok:
	#dodanie do sumy
	add %rax, %r10
	
	#zwiększenie licznika dodanych znaków
	inc %r11

skip:
	#sprawdzanie zakresu <a,b>
	dec %rdx
	jnz loop_start

end_loop:
	# zwracanie wartości
	mov %r10, %rax
	movq %r11, (%r8)
	ret
